using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.context;
using tech_test_payment_api.models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {

        private readonly PagamentoContext _context;

        public VendaController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpPost("Criar")]
        public IActionResult Criar(Venda venda)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Erro = "Cadastro com informações inválidas." });

            var vendedorBanco = _context.Vendedores.Find(venda.VendedorId);

            if (vendedorBanco == null)
                return BadRequest(new { Error = "Vendedor não encontrado." });

            if (venda.VendaItems.Count == 0)
                return BadRequest(new { Error = "Informe no mínimo um item na venda." });

            venda.Status = VendaStatus.AguardandoPagamento;

            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = venda.Id }, venda);
        }

        [HttpPost("Pagar/{id}")]
        public IActionResult Pagar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (vendaBanco.Status == VendaStatus.PagamentoAprovado)
                return BadRequest(new { Error = "Esta venda já foi paga." });
            else if (vendaBanco.Status != VendaStatus.AguardandoPagamento)
                return BadRequest(new { Error = "Somente venda que esteja aguardando pagamento pode ser paga." });

            vendaBanco.Status = VendaStatus.PagamentoAprovado;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            _context.Entry(vendaBanco).Collection(v => v.VendaItems).Load();

            return Ok(vendaBanco);
        }

        [HttpPost("Cancelar/{id}")]
        public IActionResult Cancelar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (vendaBanco.Status == VendaStatus.Cancelada)
                return BadRequest(new { Error = "Esta venda já foi cancelada" });
            else if ((vendaBanco.Status == VendaStatus.EnviadoParaTransportadora) ||
                (vendaBanco.Status == VendaStatus.Entregue))
                return BadRequest(new { Error = "Não é permitido cancelar esta venda." });

            vendaBanco.Status = VendaStatus.Cancelada;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            _context.Entry(vendaBanco).Collection(v => v.VendaItems).Load();

            return Ok(vendaBanco);
        }

        [HttpPost("EnviarParaTransportadora/{id}")]
        public IActionResult EnviarParaTransportadora(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (vendaBanco.Status == VendaStatus.EnviadoParaTransportadora)
                return BadRequest(new { Error = "Esta venda já foi enviada para transportadora." });
            else if (vendaBanco.Status != VendaStatus.PagamentoAprovado)
                return BadRequest(new { Error = "Realize o pagamento para enviar para transportadora." });

            vendaBanco.Status = VendaStatus.EnviadoParaTransportadora;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            _context.Entry(vendaBanco).Collection(v => v.VendaItems).Load();

            return Ok(vendaBanco);
        }

        [HttpPost("Entregar/{id}")]
        public IActionResult Entregar(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            if (vendaBanco.Status == VendaStatus.Entregue)
                return BadRequest(new { Error = "Esta venda já foi entregue." });
            else if (vendaBanco.Status != VendaStatus.EnviadoParaTransportadora)
                return BadRequest(new { Error = "Somente venda enviaa para transportadora pode ser entregue." });

            vendaBanco.Status = VendaStatus.Entregue;
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();

            _context.Entry(vendaBanco).Collection(v => v.VendaItems).Load();

            return Ok(vendaBanco);
        }

        [HttpGet("ObterPorId/{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendaBanco = _context.Vendas.Find(id);

            if (vendaBanco == null)
                return NotFound();

            _context.Entry(vendaBanco).Collection(v => v.VendaItems).Load();

            return Ok(vendaBanco);
        }

        [HttpGet("ObterTodos")]
        public IActionResult ObterTodos()
        {
            var vendasBanco = _context.Vendas.ToList();

            foreach (Venda venda in vendasBanco)
            {
                _context.Entry(venda).Collection(v => v.VendaItems).Load();
            }

            return Ok(vendasBanco);
        }
    }

}