using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.context;
using tech_test_payment_api.models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {

        private readonly PagamentoContext _context;

        public VendedorController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            if (!ModelState.IsValid)
                return BadRequest(new { Erro = "Cadastro com informações inválidas." });

            _context.Add(vendedor);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterPorId), new { id = vendedor.Id }, vendedor);
        }

        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if (vendedorBanco == null)
                return NotFound();

            return Ok(vendedorBanco);
        }

        [HttpGet()]
        public IActionResult ObterTodos()
        {
            var vendedoresBanco = _context.Vendedores.ToList();
            return Ok(vendedoresBanco);
        }
    }
}