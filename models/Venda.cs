using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.models
{
    public class Venda
    {
        public int Id { get; set; }
        [Required]
        public DateTime Data { get; set; }
        [Required]
        public VendaStatus Status { get; set; }
        [Required]
        public int VendedorId { get; set; }

        public virtual List<VendaItem> VendaItems { get; set; }

        public string StatusDescricao { get { return Status.ToString(); } }
        public decimal ValorTotal { get { return VendaItems?.Count == 0 ? 0 : VendaItems.Sum(v => v.Valor); } }
        public int QuantidadeItens { get { return VendaItems.Count; } }
    }
}