namespace tech_test_payment_api.models
{
    public enum VendaStatus
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}